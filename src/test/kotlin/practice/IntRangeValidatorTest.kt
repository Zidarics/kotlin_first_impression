package practice

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import practice.IntRangeValidator

internal class IntRangeValidatorTest {

    @Test
    fun input2Value() {
        val validator = IntRangeValidator(errorMessage = "QQQ", min = 0, max=42)
        assertThat(validator.input2Value("42")).isEqualTo(42)
    }

    @Test
    fun compareTo() {
        val validator = IntRangeValidator(min = 0, max=42)
        assertThat(validator.compareTo(42,42)).isEqualTo(0)
        assertThat(validator.compareTo(0, 42)).isLessThan(0)
        assertThat(validator.compareTo(42, 0)).isGreaterThan(0)
        assertThat(validator.compareTo(null, null)).isEqualTo(0)
        assertThat(validator.compareTo(null,42)).isGreaterThan(0)
        assertThat(validator.compareTo(42,null)).isLessThan(0)
    }

}