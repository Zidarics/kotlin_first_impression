package practice

/*

- it is precisely defined whether a variable or class property can be null
- inverse variable declaration order val i:Int
- it is precisely defined whether a variable can be modified  val/var
- no primitve types (byte,short,int,long,float,double), it is in compiler scope
- print a variable or expression directly in a string
- can be more than one classes in a .kt file
- getter / setter automatically exists for class property
- extension functions (function is not a member of a class)
- default value of a function argument or a class property
- operator elvis behaves very differently
- Void -> Unit, Object -> Any
- Raw strings
- Parameter passing to a function or class property by name=value style
 */

interface Validator<INPUT : Any, FIELD : Any> {

    val errorMessage: String?

    val min: FIELD?

    val max: FIELD?

    fun isValid(v: FIELD?): Boolean

    fun input2Value(input: INPUT?): FIELD?

    fun isRange(): Boolean
}

interface RangeValidator<INPUT : Any, FIELD : Any>
    : Validator<INPUT, FIELD> {

    fun compareTo(a: FIELD?, b: FIELD?): Int
}

abstract class AbstractValidator<INPUT : Any, FIELD : Any>(
    override val errorMessage: String?,
    override val min: FIELD? = null,
    override val max: FIELD? = null
) : Validator<INPUT, FIELD> {

    override fun isValid(v: FIELD?): Boolean = v != null

    override fun isRange(): Boolean = false
}

abstract class AbstractRangeValidator<INPUT : Any, FIELD : Any>(
    errorMessage: String?,
    override var min: FIELD? = null,
    override var max: FIELD? = null
) : AbstractValidator<INPUT, FIELD>(errorMessage, min, max), RangeValidator<INPUT, FIELD> {

    override val errorMessage : String?
        get() =
            when {
                min != null && max != null -> getMinMaxErrorMessage()
                max != null -> getMaxOnlyErrorMessage()
                min != null -> getMinOnlyErrorMessage()
                else -> super.errorMessage
            }

    override fun isValid(v: FIELD?): Boolean {
        v?.let { test->
            min?.let { mn-> // min is not null
                if (compareTo(test, mn) >= 0) {  // value >= min
                    max?.let { mx-> // max is not null
                        return compareTo(test,mx) <=0   // ok between min & max
                    }
                    return true
                }
                return false  //min is not null but < less than min
            }
            max?.let { mx->// min is null, but max is not
                return compareTo(test,mx) <= 0
            }
            return true //min & max is null, but v is not
        }
        return false // value is null
    }

    override fun isRange(): Boolean = true

    abstract fun getMinOnlyErrorMessage(): String

    abstract fun getMaxOnlyErrorMessage(): String

    abstract fun getMinMaxErrorMessage(): String
}

class IntRangeValidator(
    errorMessage: String?="Mandatory field",
    min : Int?,
    max: Int?
) : AbstractRangeValidator<String, Int>(
    errorMessage = errorMessage,
    min = min,
    max = max
) {
    override fun input2Value(input: String?): Int? {
        return input?.let {
            input.toInt()
        }
    }

    override fun compareTo(a: Int?, b: Int?): Int {
        a?.let {va-> // a is not null
            b?.let { vb-> // b is not null
                return va.compareTo(vb)
            }
            return -1 // b is null
        }
        b?.let { // a is null but b is not
            return 1
        }
        return 0 // both are null
    }

    override fun getMinOnlyErrorMessage(): String {
        return "value must be larger than $min"
    }

    override fun getMaxOnlyErrorMessage(): String {
        return "value must be less than $max"
    }

    override fun getMinMaxErrorMessage(): String {
        return "value must be between $min and $max"
    }

}
