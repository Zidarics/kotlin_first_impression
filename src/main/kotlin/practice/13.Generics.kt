package practice


/*

https://proandroiddev.com/understanding-generics-and-variance-in-kotlin-714c14564c47

Java:   extends      super
Kotlin: out          in
 */
class Consumer<in T> {
    fun consume(t:T) { println("Hey $t! What do you want to buy today ?") }
}

fun charSequenceConsumer() : Consumer<CharSequence> {
    val csc : Consumer<CharSequence> = Consumer()
    return csc
}

fun inProjection() {
    //TODO
    val name : CharSequence = "Dumb consumer"
    val stringConsumer : Consumer<String> = charSequenceConsumer() // ok since in projection
    //val anyConsumer : Consumer<Any> = charSequenceConsumer() // error, Any cannot be passed
    //val outConsumer : Consumer<out CharSequence> =  // Error T is in parameter
}

fun outProjection() {
    val takeList : MutableList<out Ship> = mutableListOf() // Java: List<? extends Ship>
    val ship = takeList[0]
    // takeList.add(Titanic) // Error., lower bound for generci is not specified
    //TODO

}

class Generics {
    companion object {
        @JvmStatic fun main(args:Array<String>) {
            inProjection()
            outProjection()
        }
    }
}