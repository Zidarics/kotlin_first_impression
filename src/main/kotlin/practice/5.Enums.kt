package practice


enum class Color {
    RED,GREEN,BLUE
}

enum class Color1 {
    RED {
        override val rgb:Int = 0xff0000
    },
    GREEN {
        override val rgb:Int = 0x00ff00
    },
    BLUE {
        override val rgb:Int = 0x0000ff
    };

    abstract val rgb:Int

    fun colorString() = "#%06X".format(0xffffff and rgb)
}

enum class Planet(var population:Long=0) {
    EARTH(7*1000000000L),
    MARS();

    override fun toString(): String = "$name[population=$population]"
}


fun simpleEnum() {
    println(Color1.GREEN.colorString())
    println(Planet.EARTH)
    println("Mars at this moment: ${Planet.MARS}")
    Planet.MARS.population  = 30
    println("Mars at 2050: ${Planet.MARS}")
}

class Enums {
    companion object {
        @JvmStatic fun main(args:Array<String>) {
            simpleEnum()
        }
    }
}