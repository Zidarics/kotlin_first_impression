package practice

import javax.swing.text.Position

interface IfA {
    val helloWorld
        get() = "Hello world"
}

/*
https://youtrack.jetbrains.com/issue/KT-15193
 */
interface IfB {
    val helloWorld : Int
        get() =  42 //field
//        set(value) { field = value }
}


interface Named {
    val name: String
}

interface IfPerson : Named {
    val firstName: String
    val lastName: String

    override val name: String get() = "$firstName $lastName"
}

data class Employee(
        // implementing 'name' is not required
        override val firstName: String,
        override val lastName: String,
        val position: Position
) : IfPerson

//resolving overriding conflicts

/*
Interfaces A and B both declare functions foo() and bar().
Both of them implement foo(), but only B implements bar() (bar() is not marked abstract in A, because this is the default for interfaces, if the function has no body).
Now, if we derive a concrete class C from A, we, obviously, have to override bar() and provide an implementation.

However, if we derive D from A and B, we need to implement all the methods which we have inherited from multiple interfaces,
and to specify how exactly D should implement them.
This rule applies both to methods for which we've inherited a single implementation (bar()) and multiple implementations (foo())
 */
interface A {
    fun foo() { println("A") }
    fun bar()
}

interface B {
    fun foo() { println("B") }
    fun bar() { println("bar") }
}

class C : A {
    override fun bar() { println("bar") }
}

class D : A, B {
    override fun foo() {
        super<A>.foo()
        super<B>.foo()
    }

    override fun bar() {
        super.bar()
    }
}

//Properties in interface
interface IfWithProperties {
    val property : Int //abstract

    val propertyWithImplementation : String
        get() = "foo"

    fun foo() {
        println("property is $property")
    }
}

class Child : IfWithProperties {
    override val property: Int = 42
}

class Interfaces {
    companion object {
        @JvmStatic fun main(args:Array<String>) {
            val d = D()
            d.foo()
            d.bar()

            val ifWithProperties = Child()
            ifWithProperties.foo()
        }
    }
}
