package practice

// Generic arrays in Kotlin are represented by Array<T>

fun simpleArrays() {
    val empty = emptyArray<String>()

    val strings = Array(size=5, init = { "Item #$it"})  // it is a reference to current lambda variable. It can be changed like el->

    print(strings.contentToString())  // prints "[Item #0, Item #1, Item #2, Item #3, Item #4]
    println()

    strings.set(2, "Zaphod")
    println(strings[2])

    val a = arrayOf(1,2,3)
    println("array a:${a.contentToString()}")
    val b = Array(3) { it*2}
    println("array b:${b.contentToString()}")

    val nullArray = arrayOfNulls<Int>(3)

    val doubles = doubleArrayOf(42.0, 1.5, 21.0)

    println("averages of ${doubles.contentToString()} is ${doubles.average()}")
    println("getting 5th element which is out of index: ${doubles.getOrNull(5)}")
    println("sorted of doubles: ${doubles.sortedArray().contentToString()}, descending:${doubles.sortedArrayDescending().contentToString()}")
}

fun arrayIterator() {
    val asc = Array(5) { (it*it).toString() }
    for (s:String in asc)
        println(s)
}

fun main(ars:Array<String>) {
    simpleArrays()
    arrayIterator()
}
