package practice

import java.math.BigDecimal
import java.time.LocalDate
import java.time.Period




open class Person(open val name : String, open val birthDate: LocalDate, open var location:String, open var relativity:BigDecimal?= BigDecimal.ZERO) {
    companion object {
        val USERS = listOf(Person(name = "Zaphod Beeblebrox", birthDate = LocalDate.of(1970, 3, 14), location = "Universe"),
                Person(name = "Tricia McMillan", birthDate = LocalDate.of(1975, 12, 13), location="Earth"),
                Person(name = "Arthur Dent", birthDate = LocalDate.of(1977, 1, 1), location = "Earth"),
                Person(name = "Ford Prefect", birthDate = LocalDate.of(1979, 11, 17), location = "Betelgeuse")
        )
    }

    val age : Int = Period.between(birthDate, LocalDate.now()).years

    fun moveTo(newLocation:String) { location = newLocation }

    fun incrementRelativity() { relativity = relativity?.inc() }

    fun decrementRelativity() { relativity = relativity?.dec() }

    override fun toString(): String {
        return "$name ($age), currently at $location, relativity is $relativity"
    }

}

/*
Data classes:

https://kotlinlang.org/docs/reference/data-classes.html

The compiler automatically derives the following members from all properties declared in the primary constructor:

equals()/hashCode() pair;
toString() of the form "User(name=John, age=42)";
componentN() functions corresponding to the properties in their order of declaration;
copy() function

 */

data class PersonData(override val name : String, override val birthDate: LocalDate, override var location:String, override var relativity:BigDecimal?= BigDecimal.ZERO)
    : Person (name=name, birthDate = birthDate, location = location, relativity = relativity)
