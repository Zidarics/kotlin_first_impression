package practice

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

/*
Needs dependency
        <dependency>
            <groupId>org.jetbrains.kotlinx</groupId>
            <artifactId>kotlinx-coroutines-core</artifactId>
            <version>1.3.3</version>
        </dependency>

 */
fun printHello(millisecs:Long) {
    GlobalScope.launch {
        delay(millisecs)
        println("World")
    }
    print("Hello ")
    //Thread.sleep(2000L)
    runBlocking {
        delay(2000L)
    }
}

fun joinThreads() {
    runBlocking {
        val job = GlobalScope.launch {
            delay(1000L)
            println("World")
        }
        print("Hello ")
        job.join()
    }
}

fun main(args:Array<String>) {
    printHello(1000L)
    joinThreads()
}
