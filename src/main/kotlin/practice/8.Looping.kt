package practice


fun <T> standardForLoop(list:List<T>) {
    println("standardForLoop enter with $list")
    for(i in list)
        println(i)
}

fun <T> indexedFor(list:List<T>) {
    for ((index, element) in list.withIndex())
        println("$index element is $element")
}

fun <T>foreach(list:List<T>) {
    println("foreach enter, with $list")
    list.forEach{
        println(it)
    }
}

fun repeat(i:Int) {
    println("repeat enter, $i")
    repeat(i) {
        println("this line will be printed $i times")
        println("we are on the ${it+1}. loop iteration")
    }
}

fun labels(o:Int, i:Int) {
    outer@ for (i in 0..o) {
        inner@ for (j in 0..i) {
            println("outer is $i, inner is $j")
            if (i==5 && j==5) {
                break // will break the inner loop
             //   break@inner // will break the inner loop
             //   break@outer // will break the outer loop
            }
        }
    }
}

fun factorial(n:Long) : Long = if (n==0L) 1 else n* factorial(n-1)


fun forDownTo(target:Int) {
    println("forDownTo enter with $target")
    for (i in target downTo 1)
        print(" $i")
}

fun step(target:Int, stp:Int) {
    println("step enter with $target step $stp")
    for (i in 1..target step stp )
        print("$i ")
    println("\ndownto")
    for (i in target downTo 1 step stp)
        print("$i ")

    println("\nExcludes last element")
    for (i in 0 until target)
        print("$i ")

}

class Looping {

    companion object {
        val names = listOf("Zaphod", "Tricia", "Arthur")
        val numbers = (0..10).toList()

        @JvmStatic fun main(arg:Array<String>) {
            standardForLoop(names)
            standardForLoop((0..9).toList())
            indexedFor(names)
            foreach(names)
            repeat(4)
            labels(10,10)
            val fact = 5L
            println("factorial of $fact is ${factorial(fact)} ")
            val numberStrings = numbers.map { "Number $it" }
            println("$numberStrings")
            forDownTo(5)
            step(target=10, stp=2)
        }
    }
}