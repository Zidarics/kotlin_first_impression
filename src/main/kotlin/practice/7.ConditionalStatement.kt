package practice


fun when1(arg:String) {
    println("Enter when1 arg is $arg")
    when(arg) {
        "English" -> println("How are you?")
        "German" -> println("Wie geht es dir?")
        else -> println("I don't know that language yet :(")
    }
}

fun <T> when2(find:T, names:List<T>) {
    println("when2 enter, find:$find, names:$names")
    when (find) {
        in names -> println("I know this")
        !in names -> println("$find is outside")
    }
}

fun assignmentWithWhen(arg:String) :String {
    return when(arg) {
        "English" -> "How are you?"
        "German" -> "Wie geht es dir?"
        else -> "I don't know that language yet :("
    }
}

fun assignmentWithIf(arg:Int) {
    val str = if (arg==42) "Yes you know the final truth" else "You need to read the Hitchhiker's Guide To The Galaxy"
    println(str)
}

fun whenInsteadOfIfElseIfChain(arg:String?) : String {
    println("whenInsteadOfIfElseIfChain enter, arg:$arg")
    return when {
        arg == null -> "The string is null"
        arg.isEmpty() -> "The string is empty"
        arg.length > 6 -> "The string is long"
        else -> "ok"
    }
}

enum class Days {
    Sunday,
    Monday,
    Tuesday,
    Wednesday,
    Thursday,
    Friday,
    Saturday
}

fun whenWithEnum(day:Days) : String {
    println("whenWithEnum Enter with $day")
    return when(day) {
        Days.Saturday, Days.Sunday -> "Hurray we can rest!"
        Days.Monday -> "We hate the first day :("
        Days.Tuesday, Days.Wednesday, Days.Thursday -> "We have to work hard"
        Days.Friday-> "We are waiting for weekend"
    }
}

class ConditionalStatement {

    companion object {
        @JvmStatic fun main(args:Array<String>) {
            when1("English")
            when1("German")
            when1("Magyar")

            when2("Zaphod", listOf("Zaphod", "Tricia", "Arthur"))
            when2("Brian", listOf("Zaphod", "Tricia", "Arthur"))
            when2(42, listOf(1,42,1000))
            when2(42, listOf(1,43,1000))
            when2(42.0, listOf(1,43,1000))
            println(assignmentWithWhen("English"))
            assignmentWithIf(42)
            assignmentWithIf(43)
            println(whenInsteadOfIfElseIfChain(null))
            println(whenInsteadOfIfElseIfChain(""))
            println(whenInsteadOfIfElseIfChain("Zaphod Beeblebrox"))
            println(whenInsteadOfIfElseIfChain("Zaphod"))

            println(whenWithEnum(Days.Saturday))
        }
    }
}