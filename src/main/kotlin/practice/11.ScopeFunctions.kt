package practice

import kotlin.random.Random


/*
 The context object is available as an argument (it). The return value is the lambda result.
 let can be used to invoke one or more functions on results of call chains.
 */
fun letExample() {
    Person.USERS.find { it.age < 42 }?.let {
        println(it)
        it.moveTo("Orion")
        it.incrementRelativity()
        println(it)
    }

    val numbers = mutableListOf("one", "two", "three", "four", "five")
    numbers.map { it.length }
            .filter { it > 3 }
            .let {
                println(it)  // and more function calls if needed
            }
    //If the code block contains a single function with it as an argument,
    // you can use the method reference (::) instead of the lambda
    numbers.map { it.length }
            .filter { it > 3 }
            .let(::println)
}

/*
 A non-extension function: the context object is passed as an argument, but inside the lambda,
 it's available as a receiver (this). The return value is the lambda result.
 We recommend with for calling functions on the context object without providing the lambda result.
 In the code, with can be read as “with this object, do the following.”
 */
fun withExample() {
    with (Person.USERS.last()) {
        println(this)
        this.moveTo("Procyon")
        this.decrementRelativity()
        println(this)
    }
}

/*
The context object is available as a receiver (this). The return value is the lambda result.
run does the same as with but invokes as let - as an extension function of the context object.
run is useful when your lambda contains both the object initialization and the computation of the return value.
 */
fun runExample() {
    with (Person.USERS.last()) {
        this.name.run {
            println("length of name is $length")
        }

        println(this)

        this.moveTo("Procyon")
        this.decrementRelativity()
        println(this)
    }

}

/*
 The context object is available as a receiver (this). The return value is the object itself.
 Use apply for code blocks that don't return a value and mainly operate on the members of the receiver object.
 The common case for apply is the object configuration.
 Such calls can be read as “apply the following assignments to the object.”
 */
fun applyExample() {
    Person.USERS.first().apply {
        println(this)
        this.moveTo("Syrius")
        this.decrementRelativity()
        println(this)
    }
}

/*
 The context object is available as an argument (it). The return value is the object itself.
 also is good for performing some actions that take the context object as an argument.
 Use also for additional actions that don't alter the object, such as logging or printing debug information.
 Usually, you can remove the calls of also from the call chain without breaking the program logic.
 When you see also in the code, you can read it as “and also do the following”.
 */
fun alsoExample() {
    Person.USERS.first().also {
        println(it)
        it.moveTo("Syrius")
        it.decrementRelativity()
        println(it)
    }
}

/*
In addition to scope functions, the standard library contains the functions takeIf and takeUnless.
These functions let you embed checks of the object state in call chains.
When called on an object with a predicate provided, takeIf returns this object if it matches the predicate.
Otherwise, it returns null. So, takeIf is a filtering function for a single object.
In turn, takeUnless returns the object if it doesn't match the predicate and null if it does.
The object is available as a lambda argument (it).
 */
fun takeXXExample() {
    val number = Random.nextInt(100)

    val evenOrNull = number.takeIf { it % 2 == 0 }
    val oddOrNull = number.takeUnless { it % 2 == 0 }
    println("even: $evenOrNull, odd: $oddOrNull")

    val str = "Hello"
    val caps = str.takeIf { it.isNotEmpty() }?.toUpperCase()
    //val caps = str.takeIf { it.isNotEmpty() }.toUpperCase() //compilation error
    println(caps)
}


/*
Delegate a method to another class
 */

interface Base {
    fun print()
}

class BaseImpl(val x: Int) : Base {
    override fun print() { println("BaseImpl, x:$x") }
}

class Derived(b: Base) : Base by b

fun delegateMethod() {
    Derived(BaseImpl(10)).print()
}

/*
Function    Object reference	Return value	Is extension function
let	        it	                Lambda result	Yes
run	        this	            Lambda result	Yes
run	        -	                Lambda result	No: called without the context object
with	    this	            Lambda result	No: takes the context object as an argument.
apply	    this	            Context object	Yes
also	    it	                Context object	Yes

Here is a short guide for choosing scope functions depending on the intended purpose:

Executing a lambda on non-null objects: let
Introducing an expression as a variable in local scope: let
Object configuration: apply
Object configuration and computing the result: run
Running statements where an expression is required: non-extension run
Additional effects: also
Grouping function calls on an object: with

https://medium.com/androiddevelopers/kotlin-standard-functions-cheat-sheet-27f032dd4326

*/
class ScopeFunctions {

    companion object {
        @JvmStatic fun main(args:Array<String>) {
            letExample()
            applyExample()
            withExample()
            runExample()
            alsoExample()
            delegateMethod()
        }
    }
}