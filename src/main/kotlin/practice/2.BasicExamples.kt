package practice

fun printHello(name:String?=null)
        // : Unit
{
    if (name!=null)             //old style Java
        println("Hello $name")

    name?.let {  println("Hello $name") }   // Kotlin like
}

fun maxNum(a:Int, b:Int) : Int = when (a>b) {
    true -> a
    else -> b
}

fun readVariables() {
    print("Give me two numbers:")
    val (a,b) = readLine()!!.split(' ')
    println("Max number is ${maxNum(a.toInt(),b.toInt())}")
}

fun double(x:Int) : Int = x*2

fun double1(x:Int) = x*2   // compiler can guesses the type of return value

fun stringTemplates() {
    val num=10
    val s="i=$num"
    println("s is: $s, s.length is ${s.length}")
}

fun compares() {
    val str1="Hello, World!"
    val str2="Hello,"+" World!"
    println("1.(true) $str1==$str2:${str1==str2}") // prints true
    println("2.(true) $str1===$str2:${str1===str2}, reason of compiler optimization")
}

fun strings() {
    var a="abc" // compiler can guesses that a is String but cannot be null
//    a=null        //compiler error

    var b : String? = "abc"
    b=null

    //Raw String
    val str1 = """
        | Hello World! 
         """.trimMargin()

    val str2 = """
        #Hello World! 
         """.trimMargin("#")
//Default margin prefix is pipe (|) character, you change it with argument margin prefix is String!

    println("""
            | Tell me and I forget.
            | Teach me and I remember.
            | Involve me and I learn.
            | (Benjamin Franklin)
            """.trimMargin())

    val str3=str1
    println("3.(false) $str1==$str2:${str1==str2}")  // == is structural equality, a==b is: a?.equals(b) ?: (b===null)
    println("4.(false) $str1===$str2:${str1===str2}") // referential equality is ===
    println("5.(true) $str1===$str3:${str1===str3}")

    val code = """
        for (c in "foo")
           print(c)
           """
    println(code)

    println("How to print \$ sign?")
}


class App {
    companion object {
        @JvmStatic fun main(args:Array<String>) {
            printHello()
            printHello("from App: Zaphod Beeblebrox")
            println("double 42 is ${double(42)}")
            stringTemplates()
            compares()
            strings()
        }
    }
}

class App1 {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            App1().run()
        }
    }

    fun run() {
        printHello("from App1: Zaphod Beeblebrox")
        readVariables()
    }

}



