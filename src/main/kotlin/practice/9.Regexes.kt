package practice

import java.lang.StringBuilder
import kotlin.text.Regex


fun nullSafely(pattern:String, inside:String) {
    println("nullSafely enter, pattern:$pattern, inside:$inside")
    val matchResult = Regex(pattern).find(inside)
    val a = matchResult?.value

    // Dangerous!
    // be careful to uncomment it!
    // println("a (but we don't care null pointers:${a!!.toUpperCase()}):")

    val b = matchResult?.value.orEmpty()
    println("a:${a?.toUpperCase()}")
    println("b: ${b.toUpperCase()}")
}

fun phoneChecker(pattern:String, inside:CharSequence) : MatchResult? {
    return Regex(pattern).find(inside)
}

fun findAll(pattern:String, inside:CharSequence) : String {
    val matchedResult = Regex(pattern).findAll(inside)
    val result=StringBuilder()
    for(matchedText in matchedResult)
        result.append(matchedText.value + " ")

    return result.toString()
}

fun matchEntire(pattern:String, inside:CharSequence) : MatchResult? {
    return Regex(pattern).matchEntire(inside)
}

class Regexes {

    companion object {

        const val RAW_PATTERN = """^.*?\d{3}-\d{3}-\d{4}.*?"""
        const val ALL_PATTERN = """\d+"""

        const val TEL_OK = "123-456-7890"
        const val TEL_WRONG = "asd-123-4567"
        const val ALL_OK = "ab12cd34ef"
        const val ALL_WRONG = "abcdef"
        const val ENTIRE_OK = "100"

        @JvmStatic fun main(args:Array<String>) {
            nullSafely(pattern = "aph|ric", inside = "Zaphod BeebleBrox")
            nullSafely(pattern = "Brian", inside = "Tricia Mc Millan")
            println("try to find pattern $RAW_PATTERN in $TEL_OK : ${phoneChecker(RAW_PATTERN, TEL_OK)?.value}")
            print("try to find pattern $RAW_PATTERN in $TEL_WRONG: ")
            phoneChecker(RAW_PATTERN, TEL_WRONG)?.let {
                println(it)
            }?: println("not found")
            println("find all $ALL_PATTERN in $ALL_OK is:${findAll(ALL_PATTERN, ALL_OK)}")
            println("find all $ALL_PATTERN in $ALL_WRONG is:${findAll(ALL_PATTERN, ALL_WRONG)}")

            print("Match $ALL_PATTERN entire $ENTIRE_OK is ")
            matchEntire(pattern = ALL_PATTERN, inside = ENTIRE_OK)?.let {
                println(it.value)
            }?: println("not found")

            print("Match $ALL_PATTERN entire $ALL_WRONG is ")
            matchEntire(pattern = ALL_PATTERN, inside = ALL_WRONG)?.let {
                println(it)
            }?: println("not found")

        }

    }
}