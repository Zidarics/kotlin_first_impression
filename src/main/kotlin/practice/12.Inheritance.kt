package practice

import java.lang.UnsupportedOperationException
import java.math.BigDecimal
import java.time.LocalDate

class Vogon(name:String,
            birthDate:LocalDate,
            location:String,
            relativity:BigDecimal)
    : Person(name = name,
             birthDate = birthDate,
             location = location,
             relativity = relativity) {

    constructor(name: String, birthDate: LocalDate) : this(name=name, birthDate = birthDate, location = "unknown", relativity = BigDecimal.ZERO)

    fun demolit() {
        println("I need to build a highway through your planet!")
    }
}


fun vogon() {
    val prostaticVJ = Vogon(name = "Prostatic Vogon Jeltz",
                            birthDate = LocalDate.of(1900, 1,1),
                            location = "Vogaria",
                            relativity = BigDecimal.ZERO)
    with (prostaticVJ) {
        moveTo("Earth")
        demolit()
    }

    val otherVogon = Vogon(name = "Stamper", birthDate = LocalDate.of(1800, 1,1))
    otherVogon.moveTo("Vogaria")

}

/*
Overriding properties
 */
abstract class Car {
    abstract val name : String
    open var speed : Int = 0
}

class BrokenCar(override val name:String) : Car() {
    override var speed: Int
        get() = 0
        set(value) {
            throw UnsupportedOperationException("The car is broken")
        }
}

fun cars() {
    val car = BrokenCar("Trabant")
    car.speed=10
}

/*
Overriding methods
 */

interface Ship {

    fun sail()

    fun sink()
}

object Titanic : Ship {
    var canSail = true

    override fun sail() {
        sink()
    }

    override fun sink() {
        canSail = false
    }
}

class ClassInheritance {
    companion object {
        @JvmStatic fun main(args:Array<String>) {
           vogon()
           cars()
        }
    }
}