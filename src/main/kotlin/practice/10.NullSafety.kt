package practice

import java.lang.IllegalArgumentException

fun printIt(s:String) {
    println("prinIt:$s")
}

fun nullArg(arg:String?) {
    //Java style
    if (arg!=null) {
        printIt(arg)
    }
    else
        printIt("Arg is null")

    //Kotlin style
    arg?.let {
        printIt(it)
    } ?: printIt("Arg is null")

    // Simple assert! Dangerous
    printIt(arg!!)
}

fun eliminateNUll() {
    val a : List<Int?> = listOf(1,42,3,null)
    val b = a.filterNotNull()
    println("result of filterNotNull is:$b")
}

fun nullCoalescing() {
    var a : List<String>?=null
    val b = a?.size ?: -1
    val value : String = a?.first() ?: "Nothing there"

    //calling multiple statements of a null checked object
    a?.apply {
        //inside apply a referenced by this
        println(this.size)
    }

    //to bring a nullable variable into scope as a non-nullable reference without making it the implicit receiver of function and property calls you can use let instead of apply
    a?.let {
        // you can refer a as it inside let
        println(it.size)
    }

    val value1 : String = a?.last () ?: throw IllegalArgumentException("Value cannot be null!")

    // uncomment it, why a? is unnecessary (because throw) compiler guesses!
     val c = a?.size ?: -1
}

class NullSafety {

    companion object {
        @JvmStatic fun main(ars:Array<String>) {
            val str : String? = "Zaphod"
            nullArg(str)
            eliminateNUll()
        }
    }
}