package practice

class OldSharedRegistry {
    companion object {
        val instance = OldSharedRegistry()
    }

    private constructor() {}

    fun register(key:String, value:Any) {
        System.setProperty(key, value.toString())
    }
}


// object means it is singleton. Cannot instatiated, only one instance exists
object SharedRegistry {

    fun register(key:String, value:Any) {
        System.setProperty(key, value.toString())
    }
}

fun main(args:Array<String>) {
    // val ossr = OldSharedRegistry()

    OldSharedRegistry.instance.register("a","apple")

    //val shr = SharedRegistry()
    SharedRegistry.register("a", "apple")
}
