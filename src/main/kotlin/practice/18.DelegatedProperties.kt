package practice

import kotlin.properties.Delegates
import kotlin.reflect.KProperty


class Example {

    var p: String by Delegate()

    override fun toString() = "Example Class"
}

class Delegate() {
    operator fun getValue(thisRef: Any?, prop: KProperty<*>): String {
        return "$thisRef, thank you for delegating '${prop.name}' to me!"
    }

    operator fun setValue(thisRef: Any?, prop: KProperty<*>, value: String) {
        println("$value has been assigned to ${prop.name} in $thisRef")
    }
}

class DelegateProperties {

    var foo : Int by Delegates.observable(1) {
        property, oldValue, newValue -> println("${property.name} was changed from $oldValue, to $newValue")
    }
}


fun main(args:Array<String>) {

    val ex = Example()
    ex.p = "Ford Prefect"
    println(ex)

    val dp = DelegateProperties()
    dp.foo = 2

}
