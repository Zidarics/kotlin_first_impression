package practice

import java.time.Duration
import java.time.Instant

fun lists() {
    val list = listOf("Item 1", "Item 2", "Item 3")  // constant list
    println("list is $list")
}

fun maps() {
    val map = mapOf(Pair(1, "Item 1"), Pair(2, "Item 2"), Pair(3, "Item 3"))
    println("map:$map")
}

fun sets() {
    val set = setOf(1, 42, 3)
    println("set:$set")
}

// A Kotlin object is like a class that can't be instantiated so it must be called by name. (a static class per se)
object Benchmark {
    fun realtime(arg:List<Person>, body: (persons:List<Person>)->Unit) : Duration {
        val start = Instant.now()
        try {
            body(arg)
        }
        finally {
            val end = Instant.now()
            return Duration.between(start, end)
        }
    }
}

fun benchmarking(persons:List<Person>) {
    var time = Benchmark.realtime(persons, {  // compiler suggests that move function body out of parentheses
        val youngers = it.filter { it.age < 42 }
        println("youngers than 42 are: $youngers")
    })
    println("younger list needs ${time.toNanos()/1000} us")

    val isAllowedAge = { person : Person -> person.age < 42 }  // variable contains a filtering function reference

    time = Benchmark.realtime(persons) {
        val youngers = it.filter(isAllowedAge)
        println("youngers than 42 are: $youngers")
    }
    println("younger list with isAllowed needs ${time.toNanos()/1000} us")
}

fun main(args:Array<String>) {
    lists()
    maps()
    sets()
    benchmarking(Person.USERS)
}
