package practice

import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.core.Logger
import java.math.BigDecimal
import java.time.LocalDate

fun addTo(x:Int) = x + 2

fun sayMyName(name:String) : String {
    return "Your name is $name"
}

fun sayMyNameShorthand(name:String) : String = "Your name is $name"

fun sayMyNameShorthandNonPlusUltra(name:String) = "Your name is $name"

inline fun sayMyNameInline(name: String) = "Your name is $name"

data class Actor(val firstName:String, val lastName:String)

//Extension function
fun Actor.fullName() : String = "${this.firstName} $lastName"

//infix function
//Functions marked with the infix keyword can also be called using the infix notation (omitting the dot and the parentheses for the call).
//They must be member functions or extension functions;
//They must have a single parameter;
//The parameter must not accept variable number of arguments and must have no default value.
infix fun Int.shiftLeft(x: Int): Int= this.shl(x)

fun Int.shiftLeftOf(x: Int): Int= this.shl(x)


/*Lambda functions are anonymous functions which are usually created during a function call to act as a function parameter
They are declared by surounding expressions with {braces}.
if arguments are needed, these are put before an arrow ->
The last statement inside a lambda function is automatically the return value
If the lambda function only needs one argument, then the argument list can be omitted and the single argument referred to using it instead
 */
fun lambda() {
    println(listOf(1,2,3,4).map ({ it+2 }))  // same as below, but compiler suggest that move lambda outside ()
    println(listOf(1,2,3,4).map { it+2 })
}

fun takingOtherFunctionWithoutArg(call:()->Any?) {
    println("Taking other function enter")
    call()
}

fun takingOtherFunctionWithArgs(call:(arg1:Int, arg2:Double)->Any?) {
    println("takingOtherFunctionWithArgs enter")
    call(42, 42.0)
}

fun takingOtherFunctionWithReturnValue(call:(arg:Int)->Int) {
    val q = call(42)
    println("return value of func is $q")
}

//varargs must be the last parameter in the parameter list
fun varArgs(vararg numbers:Int) {
    println("varArgs, enter")
    for(number in numbers)
        println(number)
}

fun parameterPassingByNameValuePair() {
// It doesn't works with Java methods or Objects (LocalDate)
//    val zaphod = Person(name = "Zaphod Beeblebrox",
//                        birthDate = LocalDate.of(year=2000, month=4, day=1),
//                        location = "Betelgeuse",
//                        relativity = BigDecimal.ONE))
    val zaphod = Person(name = "Zaphod Beeblebrox",
                        birthDate = LocalDate.of(2000, 4, 1),
                        location = "Betelgeuse",
                        relativity = BigDecimal.ONE)
    val tricia = Person(relativity = BigDecimal.TEN,
                        location = "Earth",
                        name = "Tricia McMillan",
                        birthDate = LocalDate.of(2000, 4, 1))
    println("zaphod is $zaphod, tricia:$tricia")
}

class SelectiveLogging(val name:String?="Unknown") {
    //If you want to use it in DEBUG mode temporally.
    // Must be beginnig with DEBUG_TEMPORARY because git can set it to false when commit
    private val DEBUG_TEMPORARY_TESTING = true

    private val LOG = LogManager.getLogger(SelectiveLogging::class.java.name)

    private inline fun logd(msg: String) {
        if (DEBUG_TEMPORARY_TESTING) LOG.debug(msg)
    }

    init {
        logd("name in constructor is $name")
    }
}


class Functions {
    companion object {
        @JvmStatic fun main(args:Array<String>) {
            val res = listOf(1,2,3,4).map {addTo(it) }  // add 2 to all elements
            println("res:$res")
            val notWorking = listOf(1,2,3,4).map { ::addTo }  // not working contains function references
            println("notWorking:$notWorking")

            println(sayMyName("Zaphod Beeblebrox"))
            println(sayMyNameShorthand("Tricia McMillan"))
            println(sayMyNameShorthandNonPlusUltra("Arthur Dent"))
            println(sayMyNameInline("Marvin The Robot"))

            val zaphodBeeblebrox = Actor(firstName = "Zaphod",
                                         lastName = "Beeblebrox")
            println("Firstname is ${zaphodBeeblebrox.firstName}")
            println("Lastname is ${zaphodBeeblebrox.lastName}")
            println("Fullname is ${zaphodBeeblebrox.fullName()}")

            println("shift left 2 of 3 is ${2.shiftLeft(3)}")
            println("shiftLeftOf 2 of 3 is ${2.shiftLeftOf(3)}")

            lambda()

            takingOtherFunctionWithoutArg { print("I am a funcion") }
            takingOtherFunctionWithArgs{ a1, a2 -> println("I am a function with args $a1, $a2")}
            takingOtherFunctionWithReturnValue { println("my arg is $it")
                it*2
            }
            varArgs(0,1)
            varArgs(0,1,20,300,4000)
            //spread operator unpack array to a list of values
            val numbers = intArrayOf(1,42,1000)
            varArgs(*numbers)

            parameterPassingByNameValuePair()

            SelectiveLogging()
            SelectiveLogging("Zaphod")
        }
    }
}



