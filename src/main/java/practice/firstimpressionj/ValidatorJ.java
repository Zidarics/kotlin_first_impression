package practice.firstimpressionj;

public interface ValidatorJ<INPUT, FIELD> {

        String getErrorMessage();

        void setErrorMessage(String v);

        FIELD getMin();

        void setMin(FIELD v);

        FIELD getMax();

        void setMax(FIELD v);

        boolean isValid(FIELD v);

        FIELD input2Value(INPUT input);

        boolean isRange();
}


