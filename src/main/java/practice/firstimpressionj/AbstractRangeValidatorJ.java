package practice.firstimpressionj;

public abstract class AbstractRangeValidatorJ<INPUT, FIELD> extends AbstractValidatorJ<INPUT,FIELD> implements RangeValidatorJ<INPUT,FIELD> {

    private FIELD min;
    private FIELD max;

    abstract String getMinOnlyErrorMessage();

    abstract String getMaxOnlyErrorMessage();

    abstract String getMinMaxErrorMessage();

    @Override
    public  boolean isValid(FIELD v) {
        if (v==null)
            return false;

        if (min != null) {
            if (compareTo(v, min) >= 0) {
                if (max != null)
                    return compareTo(v, max) <= 0;

                return true;
            }
        }
        if (max != null)
            return compareTo(v, max) <= 0;

        return true;
    }

    @Override
    public boolean isRange(){ return true; }

    @Override
    public String getErrorMessage() {
        if (min != null && max != null)
            return getMinMaxErrorMessage();

        if (max != null)
            return getMaxOnlyErrorMessage();

        if (min != null)
            return getMinOnlyErrorMessage();

        return "Field is mandatory";
    }

}
