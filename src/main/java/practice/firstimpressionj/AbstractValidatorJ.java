package practice.firstimpressionj;

public abstract class AbstractValidatorJ<INPUT, FIELD> implements ValidatorJ<INPUT, FIELD> {

    private String errorMessage;
    private FIELD min;
    private FIELD max;

    @Override
    public String getErrorMessage() {
        return errorMessage;
    }

    @Override
    public void setErrorMessage(String v) {
        errorMessage = v;
    }

    @Override
    public FIELD getMin() {
        return min;
    }

    @Override
    public void setMin(FIELD min) {
        this.min = min;
    }

    @Override
    public FIELD getMax() {
        return max;
    }

    @Override
    public void setMax(FIELD max) {
        this.max = max;
    }

    @Override
    public boolean isValid(FIELD v) {
        return v!=null;
    }

    @Override
    public boolean isRange() {
        return false;
    }
}
