package practice.firstimpressionj;

public interface RangeValidatorJ<INPUT, FIELD> extends ValidatorJ<INPUT, FIELD> {

    int compareTo(FIELD a, FIELD b);
}
