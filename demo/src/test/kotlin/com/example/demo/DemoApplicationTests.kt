package com.example.demo

import com.example.domain.Address
import com.example.domain.Person
import com.example.repository.PersonRepository
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.data.repository.findByIdOrNull
import org.springframework.test.context.junit.jupiter.SpringExtension

@ExtendWith(SpringExtension::class)
@EntityScan("com.example.domain")
@DataJpaTest
class DemoApplicationTests @Autowired constructor(
        val repo:PersonRepository) {

    @Test
    fun `basic entity test`() {
        val p = Person("Zaphod Beeblebrox", Address("Magrathea str. 42", "M-4242", "Betelgeuse"))
        repo.save(p)
        val found = repo.findByIdOrNull(p.id!!)
        found?.let {
            Assertions.assertThat(it.name).isEqualTo(p.name)
            Assertions.assertThat(it.address).isEqualTo(p.address)
            Assertions.assertThat(it).isEqualTo(p)
        } ?: Assertions.fail("Cannot found $p in database")


        Assertions.assertThat(repo.findAll()).hasSize(1)
    }

}
