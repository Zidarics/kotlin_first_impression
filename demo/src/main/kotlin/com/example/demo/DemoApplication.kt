package com.example.demo

import com.example.domain.Address
import com.example.domain.Person
import com.example.repository.PersonRepository
import org.slf4j.LoggerFactory
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.data.repository.CrudRepository

@SpringBootApplication(scanBasePackages = [("com.demo")])
@EnableJpaRepositories("com.example.repository")
@EntityScan("com.example.domain")
class DemoApplication {
    private val log = LoggerFactory.getLogger(DemoApplication::class.java)

    @Bean
    fun init(repository: PersonRepository) = CommandLineRunner{
        repository.save(Person(name="Zaphod Beeblebrox", address = Address(street = "Magrathea str. 42", zipCode = "M4242", city = "Betelgeuse")))
    }
}

fun main(args: Array<String>) {
    runApplication<DemoApplication>(*args)
}
