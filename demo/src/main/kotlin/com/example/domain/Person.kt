package com.example.domain

import javax.persistence.*

@Entity
@Table
data class Person (@Column(nullable = false) val name : String,
                   @OneToOne(cascade = [(CascadeType.ALL)], orphanRemoval = true, fetch = FetchType.EAGER)
              val address : Address)
    : AbstractJpaPersistable<ID>()

