package com.example.domain

import javax.persistence.Entity

@Entity
data class Address(val street : String,
              val zipCode : String,
              val city : String)
    : AbstractJpaPersistable<ID>()
