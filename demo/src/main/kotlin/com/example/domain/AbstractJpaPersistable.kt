package com.example.domain

import org.springframework.data.domain.Persistable
import java.io.Serializable
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.MappedSuperclass

/*
This results in a neat setup with valid Hibernate entities:

The Entity...

... must be annotated with the javax.persistence.Entity annotation (or be denoted as such in XML mapping, which we won't consider) ✔️
... must have a public or protected (or package-private) no-argument constructor. It may define additional constructors as well ✔️
... should not be final. No methods or persistent instance variables of the entity class may be final (technically possible but not recommended) ✔️
... may extend non-entity classes as well as entity classes, and non-entity classes may extend entity classes. Both abstract and concrete classes can be entities ✔️
... may provide JavaBean-style properties. This is not a must, i.e. setters are not necessary ✔️ (We accept not to have setters)
... must provide an identifier attribute (@Id annotation), recommended to use nullable, non-primitive, types ✔️
... needs to provide useful implementations for equals and hashCode ✔️
*/

/*
For generating noargs constructors automatically you need kotlin-maven-noarg dependency, see: pom.xml
 */
typealias ID=Long

@MappedSuperclass
abstract class AbstractJpaPersistable<T:Serializable> : Persistable<T> {

    /**
     * for generating serialversionId you need to install Kotlin Serialversion generator : https://plugins.jetbrains.com/plugin/12819-kotlin-serialversionuid-generator/
     * Tools/Kotlin-> Generate Kotlin SerialversionID
     */
    companion object {
        private const  val serialVersionUID = -121L
    }

    @Id
    @GeneratedValue
    private var id : T? = null

    override fun getId(): T? = id

    override fun isNew() = null == getId()

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is AbstractJpaPersistable<*>) return false
        return true
    }

    override fun hashCode(): Int {
        return javaClass.hashCode()
    }

}
