package com.example.repository

import com.example.domain.ID
import com.example.domain.Person
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface PersonRepository : CrudRepository<Person, ID> {

    fun getByAddressStreet(street:String) : Person?

}
